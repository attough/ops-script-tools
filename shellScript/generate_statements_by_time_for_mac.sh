#!/bin/bash
# set -x
################ Version Info ##################
# Create Date:  2022-07-25
# Author:       attough
# Mail:         attough#163.com(#替换为@)
# Version:      1.0
# Attention:   
# feature:      以时间增长为主的语句生成脚步
#               一般用来生成sql，但不限于此
################################################

# 加载环境变量 
# 如果脚本放到crontab中执行，会缺少环境变量，所以需要添加以下3行

# . /etc/profile
# . ~/.bash_profile
# . /etc/bashrc
if [ "$(uname)"=="Darwin" ] ; then
    echo
elif [ "$(expr substr $(uname -s) 1 5)"=="Linux" ] ;then
# GNU/Linux操作系统
	echo "\nerr: 当前系统为 GNU/Linux操作系统，请使用 generate_statements_by_time_for_linux.sh\n"
    exit -1
fi


# 变量定义
# 本脚本执行时当前目录
path=`pwd`
# echo $path

# 版本信息
version(){
    echo "version: 1.0"
}

# 用法说明
usage(){
    cat << EOF

Usage: $0 [options] [arg]
Options:
    -o --orgStr     原始语句 (必须 包含【{startTime}】 【{endTime}】两个占位符)
    -s --startTime  开始时间 (必须 1,开始时间 小于 结束时间; 2,形式为 %Y-%m-%d %H:%M:%S (需要带上时分秒))
    -e --endTime    结束时间 (必须 1,结束时间 小于 开始时间; 2,形式为 %Y-%m-%d %H:%M:%S (需要带上时分秒))
    -i --interval   间隔数 (必须 大于1的整数)
    -u --unit       间隔单位 [Y 年][m 月] [d 日] [H 时] [M 分] [S 秒]
    -f --format     默认是 "+%Y-%m-%d %H:%M:%S"
Examples:
    $0  -o "update t_user set name='xxx' where createTime > '{startTime}' and createTime < '{endTime}';"\\
        -s "2022-01-01 00:00:00"\\
        -e "2022-01-02 00:00:00"\\
        -i 12\\
        -u H
    
    out:    update t_user set name='xxx' where createTime > '2022-01-01 00:00:00' and createTime < '2022-01-01 12:00:00';
            update t_user set name='xxx' where createTime > '2022-01-01 12:00:00' and createTime < '2022-01-02 00:00:00';
---------------------------------------------------------------------------------------------------------------------------
    $0  -o "update t_user set name='xxx' where createTime > {startTime} and createTime < {endTime};"\\
        -s "2022-01-01 00:00:00"\\
        -e "2022-01-02 00:00:00"\\
        -i 12\\
        -u H\\
        -f "+%Y%m%d"
    
    out:    update t_user set name='xxx' where createTime > 20220101 and createTime < 20220101;
            update t_user set name='xxx' where createTime > 20220101 and createTime < 20220102;
---------------------------------------------------------------------------------------------------------------------------
EOF
}

# 核心功能方法
doCode(){
    
    # local sTimeStemp=`date -d "${argMap[s]}" +%s`
    local sTimeStemp=`date -j -f "%Y-%m-%d %H:%M:%S" "${argMap[s]}" +%s`
    if test -z ${sTimeStemp}; then
        echo "err: -s ${argMap[s]} 错误导致退出！正确输入格式为 %Y-%m-%d %H:%M:%S;例如："
        echo "2020-01-01 00:00:00" 
        usage
        exit -1
    fi
    
    # local eTimeStemp=`date -d "${argMap[e]}" +%s`
    local eTimeStemp=`date -j -f "%Y-%m-%d %H:%M:%S" "${argMap[e]}" +%s`
    if test -z ${eTimeStemp}; then
        echo "err: -e ${argMap[e]} 错误导致退出！正确输入格式为 %Y-%m-%d %H:%M:%S;例如："
        echo "2020-01-01 00:00:00"
        usage
        exit -1
    fi
    
    if [ ${sTimeStemp} -ge ${eTimeStemp} ]; then
        echo "err: 开始时间大于等于结束时间"
        usage
        exit -1
    fi

    for ((i=0; ;i+=${argMap["i"]}))
    do
        # 生成的 本次循环中的 开始时间
        # local startTime=`date -d "${argMap[s]} ${i} ${argMap[u]}" "${argMap[f]}"`
        local startTime=`date -j -f "%Y-%m-%d %H:%M:%S" -v +${i}${argMap[u]} "${argMap[s]}" "${argMap[f]}"`
        if (($?!=0 )) ;then
            echo "异常 退出"
            exit -1
        fi

        # 生成的 本次循环中的 开始时间对应的时间戳
        # sTimeStemp=`date -d "${startTime}" +%s`
        sTimeStemp=`date -j -f "${argMap[f]:1}" "${startTime}" +%s`

        # 如果 生成的开始时间 大于等于 给定的 -e 的时间（以时间戳做数值比较） 则 结束循环
        if [ ${sTimeStemp} -ge ${eTimeStemp} ]; then
            # echo "通过时间戳结束"
            exit 1
        fi

        # 以下三行是为了 生成 本次循环中的 结束时间
        local j=${i}
        ((j=i+${argMap["i"]}))

        # local endTimeStemp=`date -d "${argMap[s]} ${j} ${argMap[u]}" "+%s"`
        local endTimeStemp=`date -j -f "%Y-%m-%d %H:%M:%S" -v +${j}${argMap[u]} "${argMap[s]}" "+%s"`
	
        # local endTime=`date -d "${argMap[s]} ${j} ${argMap[u]}" "${argMap[f]}"`
        local endTime=`date -j -f "%Y-%m-%d %H:%M:%S" -v +${j}${argMap[u]} "${argMap[s]}" "${argMap[f]}"`
    	if [ ${endTimeStemp} -gt ${eTimeStemp} ]; then
        	# endTime=`date -d "${argMap[e]}" "${argMap[f]}"`
            endTime=`date -j -f "%Y-%m-%d %H:%M:%S" "${argMap[e]}" "${argMap[f]}"`

    	fi
        echo "${argMap[o]}"|sed 's/{startTime}/'"${startTime}"'/g'|sed 's/{endTime}/'"${endTime}"'/g'
        if (($?!=0 )) ;then
            echo "异常 退出"
            exit -1
        fi
            
    done

}

main() {
# echo $#

    declare -A local argMap
    while getopts "o:s:e:i:u:f:" arg
    do
        case "$arg" in
            o)  
                argMap["o"]="$OPTARG" 
                if [[ !(${OPTARG} =~ "{startTime}") ]]
                then
                    echo "不包含 【{startTime}】占位符 "
                    usage
                    exit -1
                fi
                if [[ !(${OPTARG} =~ "{endTime}") ]]
                then
                    echo "不包含 【{endTime}】占位符 "
                    usage
                    exit -1
                fi
                ;;
            s)  
                argMap["s"]="$OPTARG" 
                ;;
            e)  
                argMap["e"]="$OPTARG" 
                ;;
            i)  argMap["i"]="$OPTARG" 

                num=${OPTARG}
                nNum="${num//[^0-9]/}"
                if [[ "${num}" = "" ]]; then
                    echo "参数 -i 不能为空"
                    usage
                    exit -1
                fi

                if [[ "${num}" = "" ]] || [[ !("${num}" = "${nNum}") ]] && [[ ${num} -lt 1 ]]
                then
                    echo "err: 非法参数: -i:"${num}
                    usage
                    exit -1
                fi
                ;;
            u)  
                argMap["u"]="$OPTARG" 
                case "$OPTARG" in
                    Y);;
                    m);;
                    d);;
                    H);;
                    M);;
                    S);;
                    ?) 
                        echo "err: 非法参数: -u:"${OPTARG}
                        usage
                        exit -1
                        ;;
                esac
                ;;
            f)
                if test -n "${OPTARG}" ;then
                    if [[ !("${OPTARG}" == +*) ]] ; then
                        echo "err: -f ${OPTARG} 必须以+开头"
                        usage
                        exit -1
                    fi
                    frmt="${OPTARG}"
                    nFrm="${frmt//[^+^%^:^\-^ ^Y^m^d^H^M^S]/}"
                    xFrm="${frmt//[+%:\- YmdHMS]/}"
                    if [[ !(${frmt} == ${nFrm}) ]] ;then
                        echo "err: -f ${OPTARG}包含非法字符: "${xFrm}
                        usage
                        exit -1
                    fi
                fi
                argMap["f"]="$OPTARG" 
                ;;
            ?)
                echo "没有找到这个 option: $arg "
                usage
                exit 1
                ;;
        esac
    done
    
    local argArr=("o" "s" "e" "i" "u" "f")
    for k in ${argArr[@]};
    do
        # 如果 ${argMap[$k]} 字符串长度 为0 (-z)
        if test -z "${argMap[$k]}" ;then
            if test "$k"="f" ;then
                argMap["${k}"]=${argMap["${k}"]:-"+%Y-%m-%d %H:%M:%S"}
            else
                echo "缺少参数: -"${k}
                usage
                exit -1    
            fi
        fi
        
    done
    doCode argMap
}

# 需要把隐号加上，不然传入的参数就不能有空格
main "$@"

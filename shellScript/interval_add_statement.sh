#!/bin/bash
#set -x
################ Version Info ##################
# Create Date:  2022-07-25
# Author:       attough
# Mail:         attough#163.com(#替换为@)
# Version:      1.0
# Attention:    本脚本需要 bash version >= 4.1.2
# Feature:      对文件 f 每 i 行添加一行指定语句 s 
#               一般用来处理sql，但不限于此
################################################

# 加载环境变量 
# 如果脚本放到crontab中执行，会缺少环境变量，所以需要添加以下3行

# . /etc/profile
# . ~/.bash_profile
# . /etc/bashrc


# 变量定义
# 本脚本执行时当前目录
path=`pwd`
# echo $path

# 系统类型 1, mac os; 2, linux;
systemType=""

# 版本信息
version(){
    echo "version: 1.0"
}
tellSystem(){
    if [ "$(uname)"=="Darwin" ] ; then
        # mac os
        echo "1"
    elif [ "$(expr substr $(uname -s) 1 5)"=="Linux" ] ;then
        # GNU/Linux操作系统
        echo "2"
    fi
}
# 用法说明
usage(){
    cat << EOF
---------------------------------------------------------
Usage: $0 [options] [arg]
Options:
    -f --file       【必须】待处理的文件（最好是全路径）
    -s --sentence   【必须】待添加的语句
    -i --interval   【必须】间隔数 (必须 大于1的整数)
---------------------------------------------------------
Examples:
    对文件【/tmp/xxx.sql】做每【100】行添加一行【commit;】处理：
---------------------------------------------------------
    $0  -f "/tmp/xxx.sql"\\
        -s "commit;"\\
        -i 100

---------------------------------------------------------
EOF
}

# 核心功能方法
doCode(){
    local rn=0
    local interval=${argMap["i"]}
    cat "${argMap[f]}"|while read line
    do
        ((rn+=1))
        ((m=rn%interval))
        echo ${line}
        if [ ${m} -eq 0 ]; then
            echo "${argMap[s]}"
        fi
    done
}

main() {

    declare -A local argMap
    while getopts "f:s:i:" arg
    do
        case "$arg" in
            s)  
                argMap["s"]="$OPTARG" 
                ;;
            f)  
                # 这里的-f参数判断$myFile是否存在
                if [ ! -f "${OPTARG}" ]; then
                    echo "err: No such file or directory[${OPTARG}]"
                fi
                argMap["f"]="$OPTARG"
                ;;
            i)  argMap["i"]="$OPTARG" 
                num=${OPTARG}
                nNum="${num//[^0-9]/}"
                if [[ "${num}" = "" ]]; then
                    echo "参数 -i 不能为空"
                    usage
                    exit -1
                fi
                if [[ "${num}" = "" ]] || [[ !("${num}" = "${nNum}") ]] && [[ ${num} -lt 1 ]]
                then
                    echo "err: 非法参数: -i:"${num}
                    usage
                    exit -1
                fi
                ;;
            ?)
                echo "没有找到这个 option: $arg "
                usage
                exit 1
                ;;
        esac
    done
    
    local argArr=("s" "i" "f")
    for k in ${argArr[@]};
    do
        # 如果 ${argMap[$k]} 字符串长度 为0 (-z)
        if test -z "${argMap[$k]}" ;then
            echo "缺少参数: -"${k}
            usage
            exit -1
        fi
        
    done
    doCode argMap
}

# 需要把隐号加上，不然传入的参数就不能有空格
main "$@"
